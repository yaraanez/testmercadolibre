import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestMercadoLibre {

    @Test
    public void buscarproducto() throws InterruptedException {
        //INICIAR PROPERTIES para WINDOWS
        System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");

        //INICIAR GOOGLE CHROME (driver)
        WebDriver driver = new ChromeDriver();

        //NAVEGAR A  https://mercadolibre.com/
        driver.get("https://mercadolibre.com/");

        //CLICK EN “BOLIVIA”
        driver.findElement(By.id("BO")).click();

        Thread.sleep(1000);
        //CLICK EN “Aceptar Cookies”
        driver.findElement(By.className("cookie-consent-banner-opt-out__action--key-accept")).click();

        driver.findElement(By.id("cb1-edit")).sendKeys("Ropa");

        driver.findElement(By.id("cb1-edit")).submit();

        driver.findElement(By.cssSelector(".ui-search-filter-dl:nth-child(4) .ui-search-filter-container:nth-child(1) .ui-search-filter-name")).click();

        String result = driver.findElement(By.className("ui-search-search-result__quantity-results")).getText();

        boolean greaterThanTree = false;
        String message = "No existen al menos 3 productos como resultado";

        try {
            String[] split = result.split(" ");
            int results = Integer.parseInt(split[0]);
            greaterThanTree = results >= 3;
            if(greaterThanTree){
                message = "Existen al menos 3 productos como resultado, " + results;
            }
        } catch (Exception e) {
        }
        Assert.assertTrue(greaterThanTree);
    }
}
